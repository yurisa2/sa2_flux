<?php
class flux
{
  public $list_item;
  public $nfeFile;
  public $storeOrderList;
  public $pagination;
  public $pathListItem;
  public $imageFile;
  public $timeFile;

  public function __construct($filename)
  {
    // $this->endpoint = $endpoint;
    // $this->marketplace = $marketplace;
    $this->filename = $filename;
    $this->pathFiles = PATHFILES.$filename;
    $this->counterFile = PATHFILES."counter.json";

    // $this->setFiles();
    // $this->getFiles();
  }

  public function setFiles()
  {
    if(!file_exists(PATHFILES)) mkdir(PATHFILES);
    if($this->timeFile) {
      $this->timeFile = PATHFILES.'timer.json';
      if(!file_exists($this->timeFile)) file_put_contents($this->timeFile,json_encode([]));
    }
    if($this->nfeFile) {
      $this->labelHelper = PATHFILES.'helper_label.json';
      if(!file_exists($this->labelHelper)) file_put_contents($this->labelHelper,'');
      $this->nfeFile = PATHFILES.$this->filename.'_nfe.json';
      if(!file_exists($this->nfeFile)) file_put_contents($this->nfeFile,'');
      $this->lastNfeFile = PATHFILES.$this->filename.'_last_nfe.json';
      if(!file_exists($this->lastNfeFile)) file_put_contents($this->lastNfeFile,'');
    }
    if($this->imageFile) {
      $this->imageFile = PATHFILES.$this->filename.'_image.json';
      if(!file_exists($this->imageFile)) file_put_contents($this->imageFile,'');
    }
    if($this->pagination) {
      $this->urlPagination = PATHFILES.'pagination.json';
      if(!file_exists($this->urlPagination)) file_put_contents($this->urlPagination,json_encode('1'));
    }
    if($this->storeOrderList) {
      $this->storeOrderList = PATHFILES.$this->filename.'_CREATE.json';
      if(!file_exists($this->storeOrderList)) file_put_contents($this->storeOrderList,json_encode(array()));
    }
    if(!file_exists($this->pathFiles.'.json')) file_put_contents($this->pathFiles.'.json','');
    if(!file_exists($this->counterFile)) file_put_contents($this->counterFile,0);

    if($this->pathListItem) {
      $this->pathListItem = $this->pathFiles.'_LIST.json';
      if(!file_exists($this->pathListItem) && $this->list_item === null) return false;
      if(!file_exists($this->pathListItem)) file_put_contents($this->pathListItem,json_encode($this->list_item));
      return true;
    }
  }

  public function getFiles()
  {
    $this->last_item = json_decode(file_get_contents($this->pathFiles.'.json'));

    $this->list_item = json_decode(file_get_contents($this->pathListItem));
    $this->counter = (int)file_get_contents($this->counterFile);
  }

  public function addCounter()
  {
    $this->counter++;
    return file_put_contents($this->counterFile,$this->counter);
  }

  public function next_item($param = null)
  {
    if($param !== null) {
      $this->last_item = (array)$this->last_item;
      if($this->list_item == '' || $this->list_item == null) return false;
      foreach ($this->list_item as $key => $value) $productIds[] = $value['id'];
      $index = array_search($this->last_item['id'],$productIds);
      if(is_bool($index)) return $this->list_item[0];
      if($index+1 == count($productIds)) {
        unlink($this->pathFiles.'.json');
        unlink($this->pathListItem);
        return false;
      }
      return $this->list_item[$index+1];
    } else {
    if($this->list_item == '' || $this->list_item == null) return false;
    $index = array_search($this->last_item,$this->list_item);

    if(is_bool($index)) return $this->list_item[0];

    if(strpos($this->pathFiles,"prod")) {
      if($index+1 == count($this->list_item)) {
        unlink($this->pathFiles.'.json');
        unlink($this->pathListItem);
        return false;
      }
    } else {
      if($index+1 == count($this->list_item)) {
        return false;
        }
      }
      return $this->list_item[$index+1];
    }
  }

  public function test_next_item()
  {
    if(empty($this->list_item) && !is_bool($this->list_item)) {
      unlink($this->pathListItem);
      return false;
    }
    if(!is_array($this->list_item)) {
      unlink($this->pathListItem);
      return false;
    }

    $nextItem = array_shift($this->list_item);

    file_put_contents($this->pathListItem, json_encode($this->list_item));

    if(empty($this->list_item)) unlink($this->pathListItem);

    return $nextItem;
  }

  public function previous_item()
  {
    $index = array_search($this->list_item,$this->last_item);

    return $this->list_item[$index-1];
  }

  public function last_item()
  {
    return $this->last_item;
  }

  public function position_item($position)
  {
    return $this->list_item[$position];
  }

  public function getOrderStoreList()
  {
    $list = file_get_contents($this->storeOrderList);

    return json_decode("$list");
  }

  public function add_item($item_id)
  {
    return file_put_contents($this->pathFiles.'.json',json_encode($item_id));
  }

  public function addOrderStore($order_id)
  {
    $storeList = json_decode(file_get_contents($this->storeOrderList));
    $storeList[] = $order_id;

    $json = file_put_contents($this->storeOrderList,json_encode($storeList));
    if(!$json) return false;
  }

  public function createLabelOrder($order_id,$label)
  {
    $this->labelFile = "etiquetas/".$order_id.".pdf";
    file_put_contents($this->labelFile,$label);
    $copyLabelOrder = copy($this->labelFile,str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/conectores/pedidos/'.$order_id.".pdf");
    if(!$copyLabelOrder) return false;
  }

  public function addOrderNfe($nfe_order)
  {
    $order_ids = json_decode(file_get_contents($this->nfeFile));
    $order_ids[] = $nfe_order;
    file_put_contents($this->nfeFile,json_encode($order_ids));
  }

  public function removeOrderNfe()
  {
    $order_ids = json_decode(file_get_contents($this->labelHelper));
    $first_item = array_shift($order_ids);
    file_put_contents($this->labelHelper,json_encode($order_ids));
  }

  public function getOrderNfe()
  {
    $nfe_ids = json_decode(file_get_contents($this->nfeFile));
    return $nfe_ids;
  }

  public function getIncrement()
  {
    return file_get_contents(PATHFILES.$this->counterFile);
  }

  public function addLabelHelper($order_id)
  {
    $order_id_label = json_decode(file_get_contents($this->labelHelper));
    $order_id_label[] = array('order' => $order_id, 'time' => time());
    file_put_contents($this->labelHelper,json_encode($order_id_label));
  }

  public function getLabelHelper()
  {
    $order_id_label = json_decode(file_get_contents($this->labelHelper));
    if(is_null($order_id_label)) return false;
    $first_item = array_shift($order_id_label);
    return $first_item;
  }

  public function getPaginationProduct()
  {
    $pagination = (int)json_decode(file_get_contents($this->urlPagination));
    return $pagination;
  }
  public function updatePaginationProduct()
  {
    $pagination = $this->getPaginationProduct();
    $pagination++;
    if($pagination > 15) $pagination = 1;
    echo "Paginação: $pagination";
    file_put_contents($this->urlPagination,json_encode($pagination));
  }

  public function addTimer($timer,$value)
  {
    $timers = $this->getTimer();
    $timers[$timer] = $value;
    file_put_contents($this->timeFile,json_encode($timers));
  }

  public function getTimer()
  {
    $timers = (array)json_decode(file_get_contents($this->timeFile));
    return $timers;
  }
}
 ?>
